var request = require('supertest'),
    expect = require('chai').expect,
    express = require('express');


process.env.PORT = 9001;
var app = require('../../server.js'); 

describe('/clips', function(){

  var id;

  before(function(done) {
    request(app)
      .post('/clips')
      .send({
        title: 'おかげ横丁',
        linkUrl: 'http://www.okageyokocho.co.jp/',
        imgUrl: 'http://www.kankomie.or.jp/lsc/upfile/spot/18/21/1821_1_l.jpg'
      })
      .expect(201)
      .end(function(err, res) {
        if (err) throw err;
        id = res.body._id;
        done();
      });
  });

  after(function(done) {
    request(app)
      .delete('/clips/' + id )
      .expect(204)
      .end(function(err, res) {
        if (err) throw err;
        done();
      });
  });

  it('should return the list of Clips with json formated.', function(done){
    request(app)
      .get('/clips')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function(err, res){
        if (err) throw err;

        expect(res.body).to.have.length(1);
        expect(res.body[0]).to.have.property('_id');
        expect(res.body[0]).to.have.property('title');
        expect(res.body[0]).to.have.property('imgUrl');
        expect(res.body[0]).to.have.property('linkUrl');
        done();
      });
  });
});
