var mongoose = require('mongoose');

var Schema   = mongoose.Schema;

var ClipSchema = new Schema({
  title:  String,
  imgUrl: String,
  linkUrl: String
});

mongoose.model('Clip', ClipSchema);

