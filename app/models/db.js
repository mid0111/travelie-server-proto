var mongoose = require('mongoose');
var fs = require('fs');

// db settings
mongoose.connect('mongodb://192.168.59.103/travelie');
// Bootstrap models
var models_path = __dirname + '/';
fs.readdirSync(models_path).forEach(function (file) {
  if (~file.indexOf('.js') && 'db.js' !== file) {
    require(models_path + '/' + file);
  }
});
mongoose.connection.on('error', function (err) {
  console.log(err);
});

