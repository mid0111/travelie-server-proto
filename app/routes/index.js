var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Clip = mongoose.model('Clip');

router.route('/clips')
  .get(function(req, res, next) {
    Clip.find({}, function(err, docs) {
      res.header('Access-Control-Allow-Origin', '*')
        .status(200)
        .send(docs);
    }) ;
  })
  .post(function(req, res, next) {
    var clip = new Clip({
      title: req.body.title,
      imgUrl: req.body.imgUrl,
      linkUrl: req.body.linkUrl
    });
    clip.save(function(err, doc) {
      res.header('Access-Control-Allow-Origin', '*')
        .status(201)
        .send(doc);
    });
  });

router.param('clip', function(req, res, next, id){
  req.id = id;
  next();
});

router.route('/clips/:clip')
  .delete(function(req, res, next) {
    Clip.findById(req.id, function(err, clip) {
      if (err) throw err;
      if (!clip) res.status(404).send('The clip does not exist.');
      clip.remove(function(err, doc) {
        if (err) throw err;
        res.status(204).send();
      });
    });
  });

module.exports = router;
